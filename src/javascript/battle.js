class Battle {
  constructor(fighter1, fighter2) {
    this.fighter1 = fighter1;
    this.fighter2 = fighter2;
  }

  fight() {
    const { fighter1, fighter2 } = this;
    while (true) {
      let res = fighter1.getHitPower() - fighter2.getBlockPower();
      fighter2.state.health -= res >= 0 ? res : 0;
      if (fighter2.state.health <= 0) {
        return { fighter1, fighter2 };
      }

      res = fighter2.getHitPower() - fighter1.getBlockPower();
      fighter1.state.health -= res >= 0 ? res : 0;
      if (fighter1.state.health <= 0) {
        return { fighter1, fighter2 };
      }
    }
  }
}

export default Battle;
