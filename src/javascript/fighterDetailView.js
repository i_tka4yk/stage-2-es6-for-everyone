import Modal from './modal';
import View from "./view";

class FighterDetailView extends View {
  constructor(fighterId, fightersDetailsMap) {
    super();

    Object.assign(this, { fighterId, fightersDetailsMap });

    this.createModal();
  }

  createModal(){
    const el = Modal.instance.cntnt
    el.innerHTML='';

    const nameElement = this.createElement({ tagName: "h3" });
    nameElement.innerText = this.fightersDetailsMap.get(this.fighterId).name;

    const inputElements = ["health", "attack", "defense"].map(prop => {
        const field = this.createInput(
          prop,
          this.fightersDetailsMap.get(this.fighterId)[prop],
          "number"
        );
        this[prop] = field;
        return field;
      }
    );
    
    const btnElement = this.createElement({
      tagName: "button",
      classname: "Btn"
    });

    btnElement.innerText = "Save";
    btnElement.addEventListener("click", this.saveHandler.bind(this));

    el.append(nameElement, ...inputElements, btnElement);
    Modal.instance.toggleModal();
  }

  saveHandler = _ => {
    const health = document.getElementById("health").value;
    const attack = document.getElementById("attack").value;
    const defense = document.getElementById("defense").value;
    const fighter = this.fightersDetailsMap.get(this.fighterId)

    this.fightersDetailsMap.set(this.fighterId, { ...fighter, health, attack, defense });
    Modal.instance.toggleModal();
  };


  createInput(propertyName, value, type) {
    const inputElement = this.createElement({
      tagName: "input",
      className: "modal-input-text",
      attributes: {
        id: propertyName,
        type,
        value
      }
    });

    const labelElement = this.createElement({
      tagName: "label",
      attributes: {
        for: propertyName
      }
    });
    labelElement.innerText = propertyName + ": ";

    const propertyElement = this.createElement({ tagName: "div" });
    propertyElement.append(labelElement, inputElement);

    return propertyElement;
  }

}

export default FighterDetailView;