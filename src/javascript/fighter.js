class Fighter {
  constructor(conf){
    this.state = {...conf};
  }

  getHitPower(){
    const criticalHitChance = Math.random() + 1;
    return this.state.attack * criticalHitChance;
  }

  getBlockPower(){
    const dodgeChance = Math.random() + 1;
    return this.state.defense * dodgeChance;
  }
}

export default Fighter;