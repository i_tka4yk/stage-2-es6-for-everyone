import View from "./view";

let modal = Symbol();
let modalEnforcer = Symbol();

class Modal extends View {
  constructor(enforcer) {
    if (enforcer !== modalEnforcer)
      throw "Instantiation failed: use modal.instance() instead of new.";
    super();

    this.createModal();
  }

  static get instance() {
    if (!this[modal]) this[modal] = new Modal(modalEnforcer);
    return this[modal];
  }

  static set instance(v) {
    throw "Can't change constant property!";
  }

  createModal() {
    this.element = this.createElement({
      tagName: "div",
      className: "modal"
    });

    const modalContent = this.createElement({
      tagName: "div",
      className: "modal-content"
    });

    const closeButton = this.createElement({
      tagName: "span",
      className: "close-button"
    });
    closeButton.innerHTML = "&times;";

    this.cntnt = this.createElement({
      tagName: "div"
    });
    this.cntnt.innerText = "Bla-bla-bla!";

    modalContent.append(closeButton, this.cntnt);
    this.element.append(modalContent);
    document.body.append(this.element);

    closeButton.addEventListener("click", this.toggleModal.bind(this));
    window.addEventListener("click", this.windowOnClick.bind(this));
  }

  toggleModal() {
    this.element.classList.toggle("show-modal");
  }

  windowOnClick(event) {
    if (event.target === this.element) {
      this.toggleModal();
    }
  }
}

export default Modal;
