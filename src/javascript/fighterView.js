import View from "./view";

class FighterView extends View {
  constructor(fighter, handleClick, handleChoice) {
    super();

    this.createFighter(fighter, handleClick, handleChoice);
  }

  createFighter(fighter, handleClick, handleChoice) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const choiceElement = this.createElement({
      tagName: "input",
      attributes: { type: "checkbox", disabled: "disabled" }
    });
    choiceElement.addEventListener(
      "change",
      event => handleChoice(event, fighter),
      false
    );

    this.element = this.createElement({ tagName: "div", className: "fighter" });
    this.element.append(imageElement, nameElement, choiceElement);
    this.element.addEventListener(
      "click",
      event => handleClick(event, fighter, choiceElement),
      false
    );
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: "span",
      className: "name"
    });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: "img",
      className: "fighter-image",
      attributes
    });

    return imgElement;
  }
}

export default FighterView;
