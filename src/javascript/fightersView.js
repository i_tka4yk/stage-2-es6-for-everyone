import View from "./view";
import FighterView from "./fighterView";
import { fighterService } from "./services/fightersService";
import Modal from "./modal";
import FighterDetailView from "./fighterDetailView";
import Fighter from "./fighter";
import Battle from "./battle";

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.handleChoiceClick = this.handleFighterChoice.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  fightersChoiced = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(
        fighter,
        this.handleClick,
        this.handleChoiceClick
      );
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: "div",
      className: "fighters"
    });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter, choiceElement) {
    if (this.fightersDetailsMap.has(fighter._id)) {
    } else {
      const fighterDetails = await fighterService.getFighterDetails(
        fighter._id
      );
      this.fightersDetailsMap.set(fighter._id, fighterDetails);
    }

    choiceElement.disabled = false;

    const fighterDetail = new FighterDetailView(
      fighter._id,
      this.fightersDetailsMap
    );
  }

  handleFighterChoice(event, fighter) {
    if (event.target.checked) {
      this.fightersChoiced.set(fighter._id, fighter);
    } else {
      this.fightersChoiced.delete(fighter._id);
    }

    if (this.fightersChoiced.size === 2) {
      const choiced = [];
      this.fightersChoiced.forEach((_, key) => choiced.push(key));

      const obj1 = this.fightersDetailsMap.get(choiced[0]);
      const obj2 = this.fightersDetailsMap.get(choiced[1]);
      const fighter1 = new Fighter(obj1);
      const fighter2 = new Fighter(obj2);

      const battle = new Battle(fighter1, fighter2);
      const result = battle.fight();
      const modal = Modal.instance;
      const winner = result.fighter1.state.health > 0 ? fighter1 : fighter2;
      modal.cntnt.innerHTML = `${winner.state.name} won! 💪
        <p>
          ${fighter1.state.name} health: ${fighter1.state.health.toFixed(
        2
      )}<br/>
          ${fighter2.state.name} health: ${fighter2.state.health.toFixed(2)}
        </p>
      `;
    }
  }
}

export default FightersView;
